class ProductionConfig:
    # Common config
    # SECRET_KEY = os.environ.get('PUSH_SECRET_KEY') or 'yuxi_push_secret_key'
    ENV = 'production'
    SECRET_KEY = 'yuxi_development_push_secret_key'
    MAX_LIST_USERS = 60

    # ENV specified config
    DEBUG = False

    # Redis
    REDIS_URL = 'redis://localhost:6379/1'

    # Getui
    GETUI_ENDPOINT = 'http://sdk.open.api.igexin.com/apiex.htm'
    GETUI_APP_ID = 'xLQCHkmPON7vEobn9hU8b6'
    GETUI_APP_KEY = 'da3TZxVR1u6au2Ql5WTct'
    GETUI_APP_SECRET = 'dYoaWmOWIM8VvV3SXq9Uv5'
    GETUI_MASTER_SECRET = 'pMfj9KS8Ah6hemZOwngBo4'

    # SQLAlchemy
    SQLALCHEMY_DATABASE_URI = 'mysql://localhost:3306/push_development'
    SQLALCHEMY_TRACK_MODIFICATIONS = False


config = ProductionConfig
