from fabric.api import *
from fabric.contrib.files import *

env.user = 'root'
env.hosts = ['120.76.65.85']


def install_tools():
    run('yum install -y redis')
    run('yum install -y nginx')
    run('yum install -y python')
    run('yum install -y python-pip')
    run('pip install virtualenv')
    run('pip install supervisor')

    mysql_repo_rpm = 'mysql-community-release-el7-5.noarch.rpm'
    if not exists(mysql_repo_rpm):
        run('wget http://repo.mysql.com/mysql-community-release-el7-5.noarch.rpm')

    run('chmod 755 %s' % mysql_repo_rpm)
    if run('rpm -q mysql-community-release') != 'mysql-community-release-el7-5.noarch':
        run('rpm -ivh %s' % mysql_repo_rpm)

    run('yum install -y mysql-community-server')

    run('systemctl enable mysqld.service')
    run('systemctl start mysqld.service')
    run('systemctl enable redis.service')
    run('systemctl start redis.service')
    run('systemctl enable nginx.service')
    run('systemctl start nginx.service')


def make_env(env='development'):
    directory_name = 'yuxi-push-%s' % env
    app_directory = '/var/www/%s' % directory_name
    app_config_name = '%s_config.py' % env

    if not exists(app_directory):
        run('git clone https://github.com/HiggsYuxi/yuxi-push.git %s' % app_directory)
    else:
        with cd(app_directory):
            run('git pull')

    with cd(app_directory):
        run('cp %s config.py' % app_config_name)
        if not exists('env/bin/activate'):
            run('virtualenv env')

        with prefix('source env/bin/activate'):
            run('pip install -r requirements.txt')
            run('deactivate')


def restart_service(env='development'):
    directory_name = 'yuxi-push-%s' % env
    nginx_conf_name = 'yuxi_push_%s.conf' % env

    with cd('/var/www/%s' % directory_name):
        if exists('/tmp/supervisor.sock'):
            run('supervisorctl -c supervisor.conf reload')
        else:
            run('supervisord -c supervisor.conf')
        run('cp %s /etc/nginx/conf.d/' % nginx_conf_name)

    run('systemctl restart nginx')


def deploy(env='development'):
    make_env(env)
    restart_service(env)


def install(env='development'):
    install_tools()
    deploy(env)


def deploy_all():
    deploy('development')
    deploy('production')
