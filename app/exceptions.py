# -*- coding: utf-8 -*-

# Business error
ERR_CODE_BIND_DEVICE = 1000
ERR_CODE_UNBIND_DEVICE = 1001

# Runtime error
ERR_CODE_INVALID_PARAMETER = 2000
ERR_CODE_DATA_CORRUPT = 2001
ERR_CODE_PUSH_FAILED = 2002

ERR_MSG_COMMON = u'服务器出了点小问题哦~!请待会重试'

ERR_MSGS = {
    ERR_CODE_BIND_DEVICE: ERR_MSG_COMMON,
    ERR_CODE_UNBIND_DEVICE: ERR_MSG_COMMON,
    ERR_CODE_INVALID_PARAMETER: 'Invalid parameter.',
    ERR_CODE_DATA_CORRUPT: ERR_MSG_COMMON,
    ERR_CODE_PUSH_FAILED: 'Push failed.'
}


class PushError(Exception):
    err_code = None
    err_msg = None

    def __init__(self, err_code, err_msg=None):
        self.err_code = err_code
        if err_msg:
            self.err_msg = err_msg
        elif ERR_MSGS[err_code]:
            self.err_msg = ERR_MSGS[err_code]
        else:
            self.err_msg = ERR_MSG_COMMON


class InvalidParameterError(PushError):
    def __init__(self, err_msg=None):
        PushError.__init__(self, ERR_CODE_INVALID_PARAMETER, err_msg)


class DataCorruptError(PushError):
    def __init__(self, err_msg=None):
        PushError.__init__(self, ERR_CODE_DATA_CORRUPT, err_msg)
