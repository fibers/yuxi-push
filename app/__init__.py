# -*- coding: utf-8 -*-
import os
import logging
from flask import Flask, request, jsonify
from config import config
from igetui.RequestException import RequestException
from werkzeug.exceptions import default_exceptions, HTTPException, InternalServerError
from exceptions import PushError, ERR_MSGS, ERR_MSG_COMMON, ERR_CODE_PUSH_FAILED
from redis import StrictRedis
from igetui.igt_push import IGeTui
from logging import Formatter
from logging.handlers import TimedRotatingFileHandler

# toList api returns the user status for each user
os.environ['needDetails'] = 'true'

app = Flask(__name__)
app.config.from_object(config)

l = app.logger

r = StrictRedis.from_url(app.config['REDIS_URL'])
getui = IGeTui(app.config['GETUI_ENDPOINT'],
               app.config['GETUI_APP_KEY'],
               app.config['GETUI_MASTER_SECRET'])

timed_rotating_file_handler = TimedRotatingFileHandler('log/push', 'D', 1, 0)
timed_rotating_file_handler.suffix = "%Y-%m-%d.log"
timed_rotating_file_handler.setFormatter(Formatter('%(asctime)s %(levelname)s: %(message)s '
                                                   '[in %(pathname)s:%(lineno)d]'))
l.setLevel(logging.INFO)
l.addHandler(timed_rotating_file_handler)


@app.before_request
def log_before_request():
    path = request.path
    values = request.values
    l.info('Start "%s" with parameter "%s"', path, values)


@app.teardown_request
def log_teardown_request(e):
    path = request.path
    if e:
        l.info('End "%s" with exception %s', path, e)
    else:
        l.info('End "%s"', path)


def __make_http_json_error(e):
    l.exception(e)
    err_code = e.code if isinstance(e, HTTPException) else 500
    err_msg = e.description if isinstance(e, HTTPException) else ERR_MSG_COMMON
    response = jsonify(err_code=err_code, err_msg=err_msg)
    response.status_code = err_code
    return response


for code in default_exceptions.iterkeys():
    app.error_handler_spec[None][code] = __make_http_json_error


@app.errorhandler(PushError)
def handler_push_error(e):
    l.exception(e)
    l.error('#%d#(%s)', e.err_code, e.err_msg)

    response = jsonify(err_code=e.err_code,
                       err_msg=ERR_MSGS[e.err_code] if ERR_MSGS[e.err_code] else ERR_MSG_COMMON)
    response.status_code = 200
    return response


@app.errorhandler(RequestException)
def handler_request_exception(e):
    l.exception(e)
    l.error('#%d#(%s,request_id:%s)', ERR_CODE_PUSH_FAILED, e.value, e.getRequestId())

    response = jsonify(err_code=ERR_CODE_PUSH_FAILED,
                       err_msg=ERR_MSGS[e.err_code] if ERR_MSGS[e.err_code] else ERR_MSG_COMMON)
    response.status_code = 200
    return response


@app.errorhandler(Exception)
def handler_exception(e):
    l.exception(e)
    
    response = jsonify(err_code=500,
                       err_msg=ERR_MSG_COMMON)
    response.status_code = 500
    return response


# Register blueprint
from views.main import main_bp
from views.push import push_bp

app.register_blueprint(main_bp)
app.register_blueprint(push_bp)
