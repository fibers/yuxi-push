# -*- coding: utf-8 -*-

from flask import Blueprint, jsonify

main_bp = Blueprint('main_bp', __name__)


@main_bp.route('/status', methods=['GET'])
def status():
    return jsonify(status='OK')
