# -*- coding: utf-8 -*-

from flask import Blueprint, request, jsonify, current_app, json
from igetui.igt_message import IGtSingleMessage, IGtListMessage
from igetui.igt_push import IGeTui
from igetui.igt_target import Target
from igetui.payload.APNPayload import APNPayload, SimpleAlertMsg
from igetui.template.igt_apn_template import APNTemplate
from igetui.template.igt_notification_template import NotificationTemplate
from ..exceptions import *
from app import r, getui, l

push_bp = Blueprint('push_bp', __name__)


@push_bp.route('/register_push', methods=['POST'])
def register_push():
    user_id = request.form.get('user_id', '')
    client_id = request.form.get('client_id', '')
    device_id = request.form.get('device_id', '')
    device_type = request.form.get('device_type', '')

    if not user_id or not client_id or not device_id or not device_type or device_type not in ['ios', 'android']:
        raise InvalidParameterError(request.values)

    user_id_key = 'user_id:%s' % user_id
    r.hmset(user_id_key, {'device_id': device_id, 'client_id': client_id, 'device_type': device_type})

    return jsonify()


@push_bp.route('/unregister_push', methods=['POST'])
def unregister_push():
    user_id = request.form.get('user_id', '')

    user_id_key = 'user_id:%s' % user_id
    r.delete(user_id_key)

    return jsonify()


@push_bp.route('/push_to_user', methods=['POST'])
def push_to_user():
    user_id = request.form.get('user_id', '')
    title = request.form.get('title', '')
    text = request.form.get('text', '')
    payload = request.form.get('payload', '')

    if not user_id:
        raise InvalidParameterError('user_id:%s' % user_id)

    user_id_key = 'user_id:%s' % user_id
    user_info = r.hgetall(user_id_key)

    if not user_info:
        l.warn('No user info for user_id(%s)' % user_id)
        return jsonify()

    client_id = user_info.get('client_id')
    device_type = user_info.get('device_type')
    device_id = user_info.get('device_id')

    if device_type == 'android':
        target = Target()
        target.appId = current_app.config['GETUI_APP_ID']
        target.clientId = client_id

        message = __android_msg(current_app.config['GETUI_APP_ID'],
                                current_app.config['GETUI_APP_KEY'],
                                title,
                                text,
                                payload)
        result = getui.pushMessageToSingle(message, target)
        l.info('Push results: %s', result)

    elif device_type == 'ios':
        message = __ios_msg(text, payload)
        result = getui.pushAPNMessageToSingle(current_app.config['GETUI_APP_ID'],
                                              device_id, message)
        l.info('Push results: %s', result)

    else:
        raise DataCorruptError('user_id:%s,device_type:%s' % (user_id, device_type))

    return jsonify()


@push_bp.route('/push_to_users', methods=['POST'])
def push_to_users():
    str_user_ids = request.form.get('user_ids', '[]')
    title = request.form.get('title', '')
    text = request.form.get('text', '')
    payload = request.form.get('payload', '')

    user_ids = json.loads(str_user_ids)

    if not user_ids:
        raise InvalidParameterError('user_ids:%s' % user_ids)

    if len(user_ids) > current_app.config['MAX_LIST_USERS']:
        raise InvalidParameterError('user_ids:len(%d)', len(user_ids))

    android_targets = []
    ios_device_ids = []

    for user_id in user_ids:
        user_id_key = 'user_id:%s' % user_id
        user_info = r.hgetall(user_id_key)

        if not user_info:
            l.warn('No user info for user_id(%s)' % user_id)
            continue

        client_id = user_info.get('client_id')
        device_type = user_info.get('device_type')
        device_id = user_info.get('device_id')

        if device_type == 'android':
            target = Target()
            target.appId = current_app.config['GETUI_APP_ID']
            target.clientId = client_id

            android_targets.append(target)

        elif device_type == 'ios':
            ios_device_ids.append(device_id)

        else:
            raise DataCorruptError('user_id:%s,device_type:%s' % (user_id, device_type))

    if android_targets:
        message = __android_msg(current_app.config['GETUI_APP_ID'],
                                current_app.config['GETUI_APP_KEY'],
                                title,
                                text,
                                payload,
                                True)
        content_id = getui.getContentId(message)
        result = getui.pushMessageToList(content_id, android_targets)
        l.info('Push results: %s', result)

    if ios_device_ids:
        message = __ios_msg(text, payload, True)
        content_id = getui.getAPNContentId(current_app.config['GETUI_APP_ID'], message)
        result = getui.pushAPNMessageToList(current_app.config['GETUI_APP_ID'], content_id,
                                            ios_device_ids)
        l.info('Push results: %s', result)

    return jsonify()


@push_bp.route('/registered_users', methods=['GET'])
def registered_users():
    user_ids = []
    user_id_keys = r.keys('user_id:*')
    for user_id_key in user_id_keys:
        user_ids.append(user_id_key.lstrip('user_id:'))

    return jsonify(user_ids=user_ids)


def __android_msg(app_id, app_key, title, text, payload, is_list_message=False):
    template = NotificationTemplate()
    template.appId = app_id
    template.appKey = app_key
    template.title = title
    template.text = text
    template.logo = 'ic_launcher.png'
    template.transmissionType = 2
    template.transmissionContent = payload

    if is_list_message:
        message = IGtListMessage()
    else:
        message = IGtSingleMessage()

    message.isOffline = True
    message.offlineExpireTime = 1000 * 3600 * 12
    message.data = template

    return message


def __ios_msg(text, payload, is_list_message=False):
    alert_msg = SimpleAlertMsg()
    alert_msg.alertMsg = text

    apn = APNPayload()
    apn.alertMsg = alert_msg
    apn.badge = 1
    apn.addCustomMsg("payload", payload)

    template = APNTemplate()
    template.setApnInfo(apn)

    if is_list_message:
        message = IGtListMessage()
    else:
        message = IGtSingleMessage()

    message.isOffline = True
    message.offlineExpireTime = 500
    message.data = template

    return message
