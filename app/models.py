# -*- coding: utf-8 -*-
from app import db


class FailedUser(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, unique=True)
    timestamp = db.Column(db.DateTime)

    def __init__(self, user_id, timestamp):
        self.user_id = user_id
        self.timestamp = timestamp

    def __repr__(self):
        return '<FailedUser %r>' % self.username