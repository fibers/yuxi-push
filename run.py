#!/usr/bin/python
# -*- coding: utf-8 -*-

from app import app

if '__main__' == __name__:
    app.run(host='0.0.0.0', port=8080)
