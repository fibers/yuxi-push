class DevelopmentConfig:
    # Common config
    # SECRET_KEY = os.environ.get('PUSH_SECRET_KEY') or 'yuxi_push_secret_key'
    ENV = 'development'
    SECRET_KEY = 'yuxi_development_push_secret_key'
    MAX_LIST_USERS = 60

    # ENV specified config
    DEBUG = True

    # Redis
    REDIS_URL = 'redis://localhost:6379/0'

    # Getui
    GETUI_ENDPOINT = 'http://sdk.open.api.igexin.com/apiex.htm'
    GETUI_APP_ID = '2JCl8DfD2K8toDghXpKpN2'
    GETUI_APP_KEY = '1ZVpt2BQfy56dw7mu8pSB2'
    GETUI_APP_SECRET = 'hGdQFJNHHo8yIQtOj3dr33'
    GETUI_MASTER_SECRET = 'Ya2wkhKQMt5ToJK0pv39P8'

    # SQLAlchemy
    SQLALCHEMY_DATABASE_URI = 'mysql://localhost:3306/push_development'
    SQLALCHEMY_TRACK_MODIFICATIONS = False


config = DevelopmentConfig
